import 'jasmine';

declare global {
  namespace jasmine {
    interface Matchers<T> {
      toHaveBeenCalledWithParams: (actual: any, expected: any) => Matchers<any>;
    }
  }
}
