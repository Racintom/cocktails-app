import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {AppComponent} from '@app/app.component';
import {createMock} from 'ts-auto-mock';
import {HttpErrorResponse} from '@angular/common/http';

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [
        AppComponent,
      ],
      providers: [],
    }).compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('library works', () => {
    const z = createMock(HttpErrorResponse);
  });
});
