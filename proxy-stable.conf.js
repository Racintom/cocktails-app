const PROXY_CONFIG = require('./proxy.conf.js');

const TRUK__URL = 'http://localhost:6450';

PROXY_CONFIG['/'].target = TRUK__URL;

module.exports = PROXY_CONFIG;

