const TRUK_URL = 'http://localhost:8585';

const TRUK_PROXY_CONFIG = {
  target: TRUK_URL,
  changeOrigin: true,
  cookieDomainRewrite: true,
  secure: false,
  headers: {
    Connection: 'keep-alive',
  },
  logLevel: 'debug',
  onProxyRes: (proxyRes, req, res) => {
    proxyRes.headers['X-Real-IP'] = req.connection.remoteAddress;
    proxyRes.headers['X-Forwarded-For'] = req.connection.remoteAddress;
  },
};

const PROXY_CONFIG = {
  '/': TRUK_PROXY_CONFIG,
};

module.exports = PROXY_CONFIG;
