const PROXY_CONFIG = require('./proxy.conf.js');

const TRUK_URL = 'http://build.vm.casenetcloud.com:6420';

PROXY_CONFIG['/'].target = TRUK_URL;

module.exports = PROXY_CONFIG;

